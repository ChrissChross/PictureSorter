package main

import (
	"io/ioutil"
	"log"
	"strings"
	"os"
)

func main() {
	files, err := ioutil.ReadDir("./")
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		// IMG-20170116-WA0005.jpg
		if f.Name() != ".DS_Store" || f.Name() != "PictureSorter" {
			substrings := strings.Split(f.Name(), "-")

			if substrings[0] == "IMG" {
				year := substrings[1][0:4]
				month := substrings[1][4:6]
				_, err := os.Stat("./" + year)
				if err != nil {
					os.Mkdir(year, 0755)
				}
				_, err2 := os.Stat("./" + year + month)
				if err2 != nil {
					os.Chdir(year)
					os.Mkdir(month, 0755)
				}
				os.Chdir("..")
				os.Rename("./" + f.Name(), "./" + year + "//" + month + "//" + f.Name())
			} else if substrings[0] == "VID" {

			} else {
				// 20181222_223515.jpg
				year := substrings[0][0:4]
				month := substrings[0][4:6]
				_, err := os.Stat("./" + year)
				if err != nil {
					os.Mkdir(year, 0755)
				}
				_, err2 := os.Stat("./" + year + month)
				if err2 != nil {
					os.Chdir(year)
					os.Mkdir(month, 0755)
				}
				os.Chdir("..")
				os.Rename("./" + f.Name(), "./" + year + "//" + month + "//" + f.Name())
			}
		}
	}
}
